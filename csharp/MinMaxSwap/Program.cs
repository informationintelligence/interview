﻿using System;

namespace MinMaxSwap {
    class Program {

        static void Main(string[] args) {
            int[] test1 = new int[] { 10, 2, 7, 4 };
            int[] test2 = new int[] { 9, 3, 5, 1 };

            Console.WriteLine("------------");
            Console.WriteLine("Expecting: 2 10 7 4");
            PrintArray(MinMaxSwap(test1));

            Console.WriteLine("------------");
            Console.WriteLine("Expecting: 1, 3, 5, 9");
            PrintArray(MinMaxSwap(test2));
            Console.ReadLine();
        }

        private static int[] MinMaxSwap(int[] input) {
            return input;
        }


        private static void PrintArray(int[] input) {
            foreach(int item in input) {
                Console.Write(item.ToString() + " ");
            }
            Console.WriteLine();
        }
    }
}
