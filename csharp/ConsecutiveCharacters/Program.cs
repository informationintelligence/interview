﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Given an input string, find the longest number of identical consecutive characters in the string, and output the character.
For example, the string:
"aaabbccccddddddeef" would output "d", and "1cchf444jfk" would output "4". */
namespace ConsecutiveCharacters {
    class Program {
        static void Main(string[] args) {

            Console.WriteLine("-----------");
            Console.WriteLine("Input: aaabbccccddddddeef");
            Console.WriteLine(ConsecutiveCharacters("aaabbccccddddddeef"));

            Console.WriteLine("-----------");
            Console.WriteLine("Input: 1cchf444jfk");
            Console.WriteLine(ConsecutiveCharacters("1cchf444jfk"));

            Console.WriteLine("-----------");
            Console.WriteLine("Input: jjggggguugggiiiiiiiiiiuuugguwhhh");
            Console.WriteLine(ConsecutiveCharacters("jjggggguugggiiiiiiiiiiuuugguwhhh"));
            Console.ReadLine();
        }

        private static String ConsecutiveCharacters(String input) {
            // TODO
            return "";
        }
    }
}
