#include <stdio.h>

#include "../../util/asserts.h"

/** 
 * Please update the mean, median and mode functions such that all of 
 * the assertions in the main function are correct and pass.
 */

int mean(int *numbers, int size) {

    int total = 0;
    for (int i = 0; i < size; i++) {
        total += numbers[i];
    }
    return total / size;
}

int median(int *numbers, int size) {
    return numbers[size / 2];
}

int mode(int *numbers, int size) {
    return -1;
}

int main(int argc, char** argv) {
    assertEqualsI(1, mean((int[]){1}, 1));
    assertEqualsI(5, mean((int[]){10, 0}, 2));
    assertEqualsI(3, mean((int[]){2, 4}, 2));
    assertEqualsI(3, mean((int[]){2, 5}, 2));

    assertEqualsI(1, median((int[]){1}, 1));
    assertEqualsI(2, median((int[]){1, 2, 3}, 3));
    assertEqualsI(5, median((int[]){0, 10, 5}, 3));
    assertEqualsI(3, median((int[]){1, 2, 4, 5}, 4));

    assertEqualsI(1, mode((int[]){1}, 1));
    assertEqualsI(4, mode((int[]){1, 4, 4}, 3));
    assertEqualsI(3, mode((int[]){1, 2, 2, 4, 4}, 5));
    assertEqualsI(2, mode((int[]){1, 1, 2, 3, 3}, 5));
}
