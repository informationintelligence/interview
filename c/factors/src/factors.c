#include <stdio.h>

/**
 * Computes and prints the factors of the argument 'n'.
 * 
 * e.g. The factors of 8 are:
 * 1
 * 2
 * 4
 * 8
 * 
 * @param n
 *     The integer to calculate the factors of.
 */
void factors(int n) {
    // TODO
}

int main(int argc, char** argv) {
    printf("---------\n");
    factors(3);
    printf("---------\n");
    factors(8);
    printf("---------\n");
    factors(15);
}
