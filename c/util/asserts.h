#ifndef ASSERTS_H
#define ASSERTS_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void assertEqualsD(double expected, double actual) {
    if(fabs(expected - actual) > 0.001) {
        printf("Expected: %.5f does not equal actual: %.5f", expected, actual);
        exit(1);
    }
}

void assertEqualsI(int expected, int actual) {
    if(abs(expected - actual) > 0) {
        printf("Expected: %d does not equal actual: %d", expected, actual);
        exit(1);
    }
}

void assertEqualsF(float expected, float actual) {
    assertEqualsD((double)expected, (double)actual);
}

void printArray(int *array, int size, char **buffer) {

    int bufferSize = 100;
    *buffer = malloc(bufferSize*sizeof(char));

    (*buffer)[0] = '\0';
    strcat(*buffer, "[");

    for(int i = 0; i < size; i++) {
        int stringLengthItemSize = snprintf( NULL, 0, "%d", array[i] );
        char* intAsString = (char*)malloc( stringLengthItemSize + 1 * sizeof(char) );
        snprintf( intAsString, stringLengthItemSize + 1 * sizeof(char), "%d", array[i] );

        char commaSpace[3] = ", ";

        if(strlen(*buffer) + stringLengthItemSize + strlen(commaSpace) + 1 > bufferSize) {
            bufferSize += 100 + stringLengthItemSize;
            *buffer = (char*)realloc(*buffer, bufferSize*sizeof(char)); // Ignores that this might fail but hey ho
        }

        strcat(*buffer, intAsString);
        free(intAsString);
        intAsString = NULL;

        strcat(*buffer, commaSpace);
    }

    if(size > 0) {
        (*buffer)[strlen(*buffer) - 2] = '\0';
    }

    strcat(*buffer, "]");
}

void assertArrayEqualsI(int *expected, int *actual, int size) {

    for(int i = 0; i < size; i++) {
        if(expected[i] != actual[i]) {
            char *expectedString;
            printArray(expected, size, &expectedString);

            char *actualString;
            printArray(actual, size, &actualString);

            printf("Expected: %s does not equal actual: %s\n", expectedString, actualString);
            free(expectedString);
            expectedString = NULL;

            free(actualString);
            actualString = NULL;
            exit(1);
        }
    }
}

#endif