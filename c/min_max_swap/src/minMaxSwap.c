#include "../../util/asserts.h"

/**
 * This function swaps the minimum and maximum element in an array.
 * 
 * @param input
 *            The input array
 */
void minMaxSwap(int *input, int size) {
    // TODO
}


int main(int argc, char** argv) {
    int test1[] = {2, 10, 7, 4};
    minMaxSwap(test1, sizeof(test1)/sizeof(test1[0]));
    assertArrayEqualsI((int[]){10, 2, 7, 4}, test1, 4);

    int test2[] = {9, 3, 5, 1};
    minMaxSwap(test2, sizeof(test2)/sizeof(test2[0]));
    assertArrayEqualsI((int[]){1, 3, 5, 9}, test2, 4);
}
