def min_max_swap(input):
    return input

assert [10, 2, 7, 4] == min_max_swap([2, 10, 7, 4])
assert [1, 3, 5, 9] == min_max_swap([9, 3, 5, 1])