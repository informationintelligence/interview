def mean(numbers):
    return -1

def median(numbers):
    return numbers[len(numbers) / 2]

def mode(numbers):
    return -1


assert 1 == mean([1])
assert 5 == mean([10, 0])
assert 3 == mean([2, 4])
assert 3.5 == mean([2, 5])

assert 1 == median([1])
assert 2 == median([1, 2, 3])
assert 5 == median([0, 10, 5])
assert 3 == median([1, 2, 4, 5])

assert 1 == mode([1])
assert 4 == mode([1, 4, 4])
assert 3 == mode([1, 2, 2, 4, 4])
assert 2 == mode([1, 1, 2, 3, 3])
