def is_palindrome(input):
  return True


def step_one():
    # Your function should be able to see if a given string is a palindrome,
    # including whitespace
    assert_palindrome("racecar", True)
    assert_palindrome("level", True)
    assert_palindrome("notpalindrometon", False)


def step_two():
    # Extend your function to identify palindrome phrases
    step_one()
    assert_palindrome("eva can i stab bats in a cave", True)
    assert_palindrome("was it a rat i saw", True)
    assert_palindrome("this last phrase is not a palindrome", False)


def step_three():
    # Extend your function to identify whether a phrase contains a palindrome
    step_two()
    assert_palindrome("This level is tricky!", True)
    assert_palindrome("That is one fast racecar", True)
    assert_palindrome("No palindromes in this", False)


# Test util function
def assert_palindrome(actual, expected):
    result = is_palindrome(actual)
    assert result == expected, f'>>> FAILED: "{actual}". Expected {expected} but got {result}'
    print(f'Passed "{actual}"')


# This line tests your function. When you have the tests passing you can
# change it to stepTwo and then stepThree to see if you can extend the
# behaviour of your function.
step_one()
