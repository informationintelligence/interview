package com.caci.iig.fizzbuzz;

public class FizzBuzz {

    /**
     * This function takes an integer n, and prints out each of the values from 1 to
     * n with the following exceptions:
     * 
     * If the value is a multiple of 3, prints Fizz
     * 
     * If the value is a multiple of 5, prints Buzz
     * 
     * If the value is a multiple of both 3 and 5, prints FizzBuzz
     * 
     * fizzBuzz(8) would print
     * 1
     * 2
     * Fizz
     * 4
     * Buzz
     * Fizz
     * 7
     * 8
     * 
     * @param n
     *            The integer to calculate up to.
     */
    private static void fizzBuzz(int n) {
        // TODO
    }

    public static void main(String[] args) {
        System.out.println("------------");
        fizzBuzz(3);
        System.out.println("------------");
        fizzBuzz(8);
        System.out.println("------------");
        fizzBuzz(15);
    }

}

