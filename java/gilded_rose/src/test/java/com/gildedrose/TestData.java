package com.gildedrose;

public class TestData {


    private Item[] dayZeroItems = { new Item("+5 Dexterity Vest", 10, 20),
        new Item("Aged Brie", 2, 0),
        new Item("Elixir of the Mongoose", 5, 7),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        new Item("Conjured Mana Cake", 3, 22),
    };

    private Item[] dayOneItems = { new Item("+5 Dexterity Vest", 9, 19),
        new Item("Aged Brie", 1, 1),
        new Item("Elixir of the Mongoose", 4, 6),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 14, 21),
        new Item("Backstage passes to a TAFKAL80ETC concert", 9, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 4, 50),
        new Item("Conjured Mana Cake", 2, 21)
    };

    private Item[] dayTwoItems = { new Item("+5 Dexterity Vest", 8, 18),
        new Item("Aged Brie", 0, 2),
        new Item("Elixir of the Mongoose", 3, 5),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 13, 22),
        new Item("Backstage passes to a TAFKAL80ETC concert", 8, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 3, 50),
        new Item("Conjured Mana Cake", 1, 20)
    };

    private Item[] dayThreeItems = { new Item("+5 Dexterity Vest", 7, 17),
        new Item("Aged Brie", -1, 4),
        new Item("Elixir of the Mongoose", 2, 4),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 12, 23),
        new Item("Backstage passes to a TAFKAL80ETC concert", 7, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 2, 50),
        new Item("Conjured Mana Cake", 0, 19)
    };

    private Item[] dayFourItems = { new Item("+5 Dexterity Vest", 6, 16),
        new Item("Aged Brie", -2, 6),
        new Item("Elixir of the Mongoose", 1, 3),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 11, 24),
        new Item("Backstage passes to a TAFKAL80ETC concert", 6, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 1, 50),
        new Item("Conjured Mana Cake", -1, 17)
    };

    private Item[] dayFiveItems = { new Item("+5 Dexterity Vest", 5, 15),
        new Item("Aged Brie", -3, 8),
        new Item("Elixir of the Mongoose", 0, 2),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 25),
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 0, 50),
        new Item("Conjured Mana Cake", -2, 15) };

    public Item[][] expectedResults = {
        dayZeroItems,
        dayOneItems,
        dayTwoItems,
        dayThreeItems,
        dayFourItems,
        dayFiveItems
    };
}
