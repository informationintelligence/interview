package com.gildedrose;

import static org.junit.Assert.*;

import org.junit.Test;

public class GildedRoseTest {

    @Test
    public void testUpdateQuality() {
        TestData data = new TestData();
        // NewFeatureTestData data = new NewFeatureTestData();
        Item[][] expectedResults = data.expectedResults;
        Item[] items = expectedResults[0];
        GildedRose app = new GildedRose(items);

        for (Item[] expectedItems : expectedResults) {
            assertItemsEqual(expectedItems, items);
            app.updateQuality();
        }
    }

    private void assertItemsEqual(Item[] expectedItems, Item[] actualItems) {
        String[] expected = toStringArray(expectedItems);
        String[] actual = toStringArray(actualItems);
        assertArrayEquals(expected, actual);
    }

    private String[] toStringArray(Item[] items) {
        int length = items.length;
        String[] itemStrings = new String[length];
        for (int i = 0; i < length; i++) {
            itemStrings[i] = items[i].toString();
        }

        return itemStrings;
    }
}
