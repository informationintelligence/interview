package com.caci.iig.factors;

public class Factors {

    /**
     * Computes and prints the factors of the argument 'n'.
     * 
     * e.g. The factors of 8 are: 
     * 1
     * 2
     * 4
     * 8
     * 
     * @param n
     *            The integer to calculate the factors of.
     */
    private static void factors(int n) {
    	// TODO
    }

    public static void main(String[] args) {
        System.out.println("------------");
        factors(3);
        System.out.println("------------");
        factors(8);
        System.out.println("------------");
        factors(15);
    }
}

