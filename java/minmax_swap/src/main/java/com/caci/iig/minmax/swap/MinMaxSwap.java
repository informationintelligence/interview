package com.caci.iig.minmax.swap;

import static org.junit.Assert.assertArrayEquals;

public class MinMaxSwap {

    /**
     * This function swaps the minimum and maximum element in an array.
     * 
     * @param input
     *            The input array
     */
    private static void minMaxSwap(int[] input) {
        // TODO
    }

    public static void main(String[] args) {
        int[] test1 = new int[] { 2, 10, 7, 4 };
        minMaxSwap(test1);
        assertArrayEquals(new int[] { 10, 2, 7, 4 }, test1);

        int[] test2 = new int[] { 9, 3, 5, 1 };
        minMaxSwap(test2);
        assertArrayEquals(new int[] { 1, 3, 5, 9 }, test2);
    }

}

