# Open NLP Interview

Open NLP is a powerful open source natural language programming library.
It's an exciting technology that uses machine learning to extract entities and metadata from unstructured text.

## Project Outline

You have been provided with the skeleton of a project that we would like you to complete.
 
The project loads in the file `example.txt` as a string. Copy and paste some text into this file that you think would be interesting to perform openNLP against (Something from a news website that will contain lots of people, places etc...). 

The example text string is passed to the `extractEntitiesFromText` method in the OpenNLP class.

The constructor of this class creates the required models for the openNLP library using the binary files in the /models directory.
Note: There are more models available in /models and online. If you would like to use more feel free to, there is however plenty that can be done with the 4 provided.

## Your Task

Complete the `extractEntitiesFromText` method and perform openNLP against the text. You can, of course, create as many additional methods/classes as you require.

We want to see what solution you can provide using the openNLP library.

Using openNLP to extract entities from text is relatively simple. But it can be complicated to present this information in a relevant form.

Can associations be made between different entities? To what degree can we say the results obtained are accurate? How do we present this data to the user?

We want to see that you can do more than just use the library. We want to see you utilise it in an effective way.

Spend some time getting familiar with openNLP, then see if you can come up with a plan to use it in an interesting way.

A number of our developers have used the library before, if you need help or someone to bounce ideas off don't be afraid to ask!


## Note for the interviewer

To prepare the project ensure the master branch is selected.

`git checkout master`

And run a git reset

`git reset --hard HEAD`
