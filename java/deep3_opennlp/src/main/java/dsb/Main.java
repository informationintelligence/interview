package dsb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;


public class Main {

    public static final  String DIR = "fileIn/";
    public static final  String DEFAULT = "example.txt";
    public static final Logger log = LogManager.getLogger(Main.class);

    public static void main (String[] args) {
        //BasicConfigurator.configure(); // <-- If the system is having problems with log4j uncomment this line
        String fileName = (args.length > 0) ? args[0] : DEFAULT;

        try {
            String fileContent = TextFileReader.getFileContent(DIR + fileName);
            OpenNLP entityExtractor = new OpenNLP();
            entityExtractor.extractEntitiesFromText(fileContent);
        }
        catch(IOException e){
        	log.error(e);
        }
    }
}
