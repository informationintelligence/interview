package dsb;


import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;

public class OpenNLP {

    // Models - Use these to extract data
    private SentenceDetectorME sentenceDetector;
    private TokenizerME tokenizer;
    private NameFinderME nameFinder;
    private NameFinderME locationFinder;

    public static final Logger log = LogManager.getLogger(OpenNLP.class);

    /**
     * Load models in constructor
     */
    public OpenNLP() {
        InputStream inputStream;

        try {
            inputStream = new FileInputStream("models/en-sent.bin");
            SentenceModel model = new SentenceModel(inputStream);
            sentenceDetector = new SentenceDetectorME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for sentence detector", e);
        }

        try {
            inputStream = new FileInputStream("models/en-token.bin");
            TokenizerModel model = new TokenizerModel(inputStream);
            tokenizer = new TokenizerME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for Tokenizer", e);
        }

        try {
            inputStream = new FileInputStream("models/en-ner-person.bin");
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            nameFinder = new NameFinderME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for name finder", e);
        }

        try {
            inputStream = new FileInputStream("models/en-ner-location.bin");
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            locationFinder = new NameFinderME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for name finder", e);
        }
    }

    public void extractEntitiesFromText(String fileContent){

        // use OpenNLP to look for entities and display as you desire
        System.out.print(fileContent);
    }
}
