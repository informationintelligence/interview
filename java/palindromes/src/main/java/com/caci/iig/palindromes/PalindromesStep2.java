package com.caci.iig.palindromes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static com.caci.iig.palindromes.Palindromes.palindromes;

public class PalindromesStep2 {

    /**
     *
     * Extend your function to can also detect palindrome phrases
     *
     */

    public static void main(String[] args) {
        assertTrue(palindromes("eva can i stab bats in a cave"));
        assertTrue(palindromes("was it a rat i saw"));
        assertFalse(palindromes("this last phrase is not a palindrome"));
    }
}

