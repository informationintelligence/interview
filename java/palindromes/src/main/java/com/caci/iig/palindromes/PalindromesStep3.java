package com.caci.iig.palindromes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static com.caci.iig.palindromes.Palindromes.palindromes;

public class PalindromesStep3 {

    /**
     *
     * Extend your function to identify palindromes (words or phrases) in a block of text.
     *
     */

    public static void main(String[] args) {
        assertTrue(palindromes("abcdracecardefgh"));
        assertTrue(palindromes("was it a palindrome, or was it a rat i saw in here"));
        assertFalse(palindromes("notpalindrometon"));
    }
}

