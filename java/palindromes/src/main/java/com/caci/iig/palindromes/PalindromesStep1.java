package com.caci.iig.palindromes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static com.caci.iig.palindromes.Palindromes.palindromes;

public class PalindromesStep1 {

    /**
     *
     * Write a function in Palindromes.java that will determine if a string (one word) is a palindrome.
     *
     */

    public static void main(String[] args) {
        assertTrue(palindromes("racecar"));
        assertTrue(palindromes("level"));
        assertFalse(palindromes("notpalindrometon"));
    }
}

