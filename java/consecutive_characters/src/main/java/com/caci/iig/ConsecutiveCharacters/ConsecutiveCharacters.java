package com.caci.iig.ConsecutiveCharacters;

import static org.junit.Assert.assertEquals;

public class ConsecutiveCharacters {

	/*
	 * Given an input string, find the longest number of identical consecutive
	 * characters in the string, and output the character. Please update the static
	 * method such that all of the assertions are correct and pass.
	 */

	public static String consecutive(String input) {
		return "";

	}

	public static void main(String[] args) {
		assertEquals("d", consecutive("aaabbccccddddddeef"));
		assertEquals("4", consecutive("1cchf444jfk"));
		assertEquals("i", consecutive("jjggggguugggiiiiiiiiiiuuugguwhhh"));
	}
}