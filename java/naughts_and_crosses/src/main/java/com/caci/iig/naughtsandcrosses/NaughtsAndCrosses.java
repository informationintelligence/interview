package com.caci.iig.naughtsandcrosses;

public class NaughtsAndCrosses {
  /**
   * 1. Implement a system that randomly populates a data structure, representing a 3x3 grid of noughts and crosses
   * 2. Write an algorithm to determine the win state of the populated grid. i.e. Noughts, Crosses, or Stalemate 
   * 3. Modify your algorithm to *play* a game of Noughts & Crosses, ending once a player has won.
   */

    public static void main(String[] args) {

    }
}

