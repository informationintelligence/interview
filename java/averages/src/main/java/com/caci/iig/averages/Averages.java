package com.caci.iig.averages;

import static org.junit.Assert.assertEquals;

public class Averages {

    /* Please update the static methods such that all of the assertions are correct and pass. */

    public static int mean(int... numbers) {

        int total = 0;
        for (int i = 0; i < numbers.length; i++) {
            total += numbers[i];
        }
        return total / numbers.length;
    }

    public static int median(int... numbers) {
        return numbers[numbers.length / 2];
    }

    public static int mode(int... numbers) {
        // TODO
        return -1;
    }

    public static void main(String[] args) {
        assertEquals(1, mean(1));
        assertEquals(5, mean(10, 0));
        assertEquals(3, mean(2, 4));
        assertEquals(3, mean(2, 5));

        assertEquals(1, median(1));
        assertEquals(2, median(1, 2, 3));
        assertEquals(5, median(0, 10, 5));
        assertEquals(3, median(1, 2, 4, 5));

        assertEquals(1, mode(1));
        assertEquals(4, mode(1, 4, 4));
        assertEquals(3, mode(1, 2, 2, 4, 4));
        assertEquals(2, mode(1, 1, 2, 3, 3));
    }
}