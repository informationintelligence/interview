#include <iostream>
#include <math.h>

void assertEquals(double expected, double actual) {
    if(fabs(expected - actual) > 0.001) {
        std::cout << "Expected: " << expected << " does not equal actual: " << actual << std::endl;
        exit(1);
    }
}

void assertEquals(int expected, int actual) {
    assertEquals(static_cast<double>(expected), static_cast<double>(actual));
}

void assertEquals(float expected, float actual) {
    assertEquals(static_cast<double>(expected), static_cast<double>(actual));
}