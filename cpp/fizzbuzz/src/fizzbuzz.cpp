#include <iostream>

/**
 * This function takes an integer n, and prints out each of the values from 1 to
 * n with the following exceptions:
 * 
 * If the value is a multiple of 3, prints Fizz
 * 
 * If the value is a multiple of 5, prints Buzz
 * 
 * If the value is a multiple of both 3 and 5, prints FizzBuzz
 * 
 * fizzBuzz(8) would print
 * 1
 * 2
 * fizz
 * 4
 * buzz
 * fizz
 * 7
 * 8
 * 
 * @param n
 *            The integer to calculate up to.
 */
void fizzBuzz(int n) {
	// TODO
}

int main(int argc, char** argv) {
	std::cout << "---------" << std::endl;
	fizzBuzz(3);
	std::cout << "---------" << std::endl;
	fizzBuzz(8);
	std::cout << "---------" << std::endl;
	fizzBuzz(15);
}
