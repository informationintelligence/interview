#include <vector>

#include "../util/asserts.hpp"

/** 
 * Please update the mean, median and mode functions such that all of 
 * the assertions in the main function are correct and pass.
 */

int mean(std::vector<int> numbers) {
    int total = 0;
    for (int i = 0; i < numbers.size(); i++) {
        total += numbers[i];
    }
    return total / numbers.size();
}

int median(std::vector<int> numbers) {
    return numbers[numbers.size() / 2];
}

int mode(std::vector<int> numbers) {
    return -1;
}

int main(int argc, char** argv) {
    assertEquals(1, mean({1}));
    assertEquals(5, mean({10, 0}));
    assertEquals(3, mean({2, 4}));
    assertEquals(3, mean({2, 5}));

    assertEquals(1, median({1}));
    assertEquals(2, median({1, 2, 3}));
    assertEquals(5, median({0, 10, 5}));
    assertEquals(3, median({1, 2, 4, 5}));

    assertEquals(1, mode({1}));
    assertEquals(4, mode({1, 4, 4}));
    assertEquals(3, mode({1, 2, 2, 4, 4}));
    assertEquals(2, mode({1, 1, 2, 3, 3}));
}