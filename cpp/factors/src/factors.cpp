#include <iostream>


/**
 * Computes and prints the factors of the argument 'n'.
 * 
 * e.g. The factors of 8 are:
 * 1
 * 2
 * 4
 * 8
 * 
 * @param n
 *     The integer to calculate the factors of.
 */
void factors(int n) {
	// TODO
}

int main(int argc, char** argv) {
	std::cout << "---------" << std::endl;
	factors(3);
	std::cout << "---------" << std::endl;
	factors(8);
	std::cout << "---------" << std::endl;
	factors(15);
}
