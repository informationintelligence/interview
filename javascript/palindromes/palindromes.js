function isPalindrome(input) {
  return true;
}

// This line tests your function. When you have the tests passing you can
// change it to stepTwo and then stepThree to see if you can extend the
// behaviour of your function.
stepOne();

function stepOne() {
  // Your function should be able to see if a given string is a palindrome,
  // including whitespace
  assertTrue("racecar");
  assertTrue("level");
  assertFalse("notpalindrometon");
}

function stepTwo() {
  // Extend your function to identify palindrome phrases
  stepOne();
  assertTrue("eva can i stab bats in a cave");
  assertTrue("was it a rat i saw");
  assertFalse("this last phrase is not a palindrome");
}

function stepThree() {
  // Extend your function to identify whether a phrase contains a palindrome
  stepTwo();
  assertTrue("This level is tricky!");
  assertTrue("That is one fast racecar");
  assertFalse("No palindromes in this");
}

// Test util functions
function assert(input, expected) {
  const result = isPalindrome(input);
  if (result !== expected) {
    console.log(
      `>>> FAILED: "${input}". Expected ${expected} but got ${result}`
    );
  } else {
    console.log(`Passed: "${input}"`);
  }
}

function assertTrue(input) {
  assert(input, true);
}

function assertFalse(input) {
  assert(input, false);
}
