const { updateQuality } = require("../src/GildedRose");
const { expectedResults } = require("./TestData");
// const { expectedResults } = require("./NewFeatureTestData");

function testUpdateQuality() {
    for (let i = 1; i < expectedResults.length; i++) {
        const actualItems = deepClone(expectedResults[i - 1]);
        const expectedItems = expectedResults[i];
        updateQuality(actualItems);
        assertItemsEqual(expectedItems, actualItems);
    }
}

function deepClone(object) {
    return JSON.parse(JSON.stringify(object));
}

function assertItemsEqual(expectedItems, actualItems) {
    for (let i = 0; i < expectedItems.length; i++) {
        const expectedItemString = JSON.stringify(expectedItems[i]);
        const actualItemString = JSON.stringify(actualItems[i]);
        if (expectedItemString !== actualItemString) {
            throw new Error(`Difference at index ${i}\r\nExpected: ${expectedItemString}\r\nActual: ${actualItemString}`);
        }
    }
}

testUpdateQuality();
