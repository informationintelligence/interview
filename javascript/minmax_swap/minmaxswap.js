function minMaxSwap(input) {
    console.log("Replace with min max swap");
}

console.log("--------");
console.log("Expecting: [2, 10, 7, 4]")
console.log(minMaxSwap([10, 2, 7, 4]));
console.log("--------");
console.log("Expecting: [1, 3, 5, 9]")
console.log(minMaxSwap([9, 3, 5, 1]));
