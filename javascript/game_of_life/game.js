// The Game of Life

// Suppose you have an M by N board of cells, where each cell is marked as alive or dead.
// This arrangement of the board is called the state, and the next board state is found according to a set of rules:
// Neighbours: each cell has eight neighbours, up, down, left, right, and along the diagonals.
// Underpopulation: a live cell with zero or one live neighbours becomes dead in the next state.
// Survival: a live cell with exactly two or three live neighbours remains alive in the next state.
// Overpopulation: a live cell with four or more live neighbours becomes dead in the next state.
// Reproduction: a dead cell with exactly three neighbours becomes alive in the next state.

// Implement an algorithm for calculating the next state of a board, given the current state.

const random = [
    ['X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', 'X', ' ', 'X'],
    ['X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', 'X', ' ', 'X'],
    ['X', ' ', 'X', ' ', ' ', 'X', ' ', 'X', 'X', ' ', 'X'],
    ['X', ' ', 'X', 'X', 'X', 'X', ' ', 'X', 'X', ' ', 'X'],
    ['X', ' ', ' ', 'X', 'X', 'X', ' ', 'X', 'X', ' ', 'X'],
    [' ', 'X', 'X', 'X', 'X', 'X', ' ', 'X', 'X', ' ', 'X'],
    [' ', 'X', 'X', 'X', ' ', 'X', ' ', ' ', 'X', ' ', 'X'],
    ['X', ' ', 'X', 'X', ' ', 'X', 'X', ' ', ' ', ' ', ' '],
    ['X', ' ', 'X', 'X', ' ', 'X', ' ', ' ', ' ', 'X', ' '],
    ['X', ' ', 'X', 'X', ' ', ' ', ' ', ' ', 'X', ' ', ' '],
    ['X', ' ', 'X', 'X', ' ', 'X', ' ', ' ', 'X', 'X', 'X'],
];

const glider = [
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', 'X', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', 'X', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', 'X', 'X', 'X', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
];

/**
 * Return the board state advanced by one iteration.
 */
const advance = (state) => {
    const newState = [] //TODO
    return newState;
};

/**
 * Runs the game of life.
 */
const run = () => {
    let state = glider;
    console.table(state);

    for (let i = 0; i < 10; i++) {
        state = advance(state);
        console.table(state);
    }
};

run();